<?php

// me conecto a db
$mysqli = mysqli_connect("localhost", "rrojas_cion", "Cion20181", "rrojas_demoguf");

if (!$mysqli) {
	echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
	die();
}

function consulta_imagenes(){
  global $mysqli;
  $resultado = $mysqli->query("SELECT * FROM `imagenes` WHERE 1");
	while ($row = mysqli_fetch_assoc($resultado)){
  	$rows[]=$row;
	}
  return $rows;
}

function consulta_stock($sabor){
  global $mysqli;
  $resultado = $mysqli->query("SELECT $sabor FROM `stock` WHERE 1");
  $stock = mysqli_fetch_assoc($resultado);
  $cantidad = $stock[$sabor];
  return $cantidad;
}

function consulta_precio($sabor){
  global $mysqli;
  $resultado = $mysqli->query("SELECT $sabor FROM `precios` WHERE 1");
  $precios = mysqli_fetch_assoc($resultado);
  $precio = $precios[$sabor];
  return $precio;
}

function descuenta_stock($cantidad,$sabor){
	  global $mysqli;
		$mysqli->query("UPDATE `stock`  SET $sabor = $sabor - $cantidad ");
}

function agrega_stock($cantidad,$sabor){
	  global $mysqli;
		$mysqli->query("UPDATE `stock`  SET $sabor = $sabor + $cantidad ");
}
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Chat Empanadas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="jumbotron">
      <h1 class="display-4">Empanadas Bot</h1>
      <p class="lead">Demo de un bot para el pedido de Empanadas desarrollado en dialogflow de Google</p>
      <hr class="my-4">
    </div>
    <section id="pricing">
        <div class="container">
          <div class="title-block">
            <h2>Imágenes enviadas</h2>
          </div>
          <div class="row">
    			<?php  $imagenes = consulta_imagenes();
    						foreach ( $imagenes as $imagen) { ?>

    						<div class="col-md-4">
    			       	<div class="pricing-box">

    								<img src="<?php echo $imagen['url'] ?>" style="width:100%" alt="">

    							</div>
    						</div>

    			<?php } ?>
          </div>
        </div>
      </section>
    <footer class="jumbotron">
      <p>&copy; Company 2018-2019</p>
    </footer>
  </body>
</html>
